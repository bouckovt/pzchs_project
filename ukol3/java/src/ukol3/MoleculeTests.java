package ukol3;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.*;


public class MoleculeTests {
	
	
	
	/**
	 * Test prepsani metody addNode(), jestli bude mozne pracovat s ulozenymi Node jako s Atomy po cast.
	 */
	@Test
	public void addNodeTest() {
		
		Molecule mol = new Molecule();
		mol.addNode("C");
		
		for (Node node : mol.getNodeMap().values()) {
			Atom atom = (Atom) node;
			Assert.assertTrue(atom instanceof Atom);
			Assert.assertTrue(node instanceof Atom);
		}
		
	}
	
	/**
	 * Test zda metoda readMolFile() vraci objekt typu Molecue.
	 * 
	 * @throws ParseException
	 * @throws IOException
	 */
	
	@Test
	public void readMolFileTest() throws ParseException, IOException {
		
		BufferedReader rd;
		rd = new BufferedReader(new FileReader("adenosine.mol"));
		
		Assert.assertTrue(Molecule.readMolFile(rd) instanceof Molecule);
	}
	
	/**
	 *  Test zda metoda readMolFile() ulozi spravne informace o atomech z molfile.
	 */
	@Test
	public void readMolFileTest2() throws ParseException, IOException {
		
		BufferedReader rd;
		rd = new BufferedReader(new FileReader("aspartic_acid.mol"));
		Molecule mol = Molecule.readMolFile(rd);
		List<String> atoms = new ArrayList<String>();
		// ocekavany vystup z tohoto souboru
		atoms.add("C 3");
		atoms.add("N 2");
		atoms.add("C 1");
		atoms.add("O 7");
		atoms.add("O 6");
		atoms.add("C 5");
		atoms.add("C 4");
		atoms.add("O 9");
		atoms.add("O 8");
		
		for (Node node : mol.getNodeMap().values()) {
			Atom atom = (Atom) node;
			String str = atom.getName() + " " + atom.getAtomIndex();
			Assert.assertTrue(atoms.contains(str));
			
		}
	}
	
	/**
	 *  Test zda metoda connectNodes() z tridy Graph vraci spravne vysledky pri pouziti na objekt tridy Molecule 
	 * @throws IOException 
	 * @throws ParseException 
	 */
	@Test
	public void connectNodesTest() {
		Molecule mol = new Molecule();
		mol.addNode("C");
		mol.addNode("C");
		
		mol.connectNodes("1", "2");
		List<String> examp1 = new ArrayList<String>();
		examp1.add("2");
		
		List<String> examp2 = new ArrayList<String>();
		examp2.add("1");
		List<String> ngbOfatom1 = mol.getNodeMap().get("1").getNeighbors();
		List<String> ngbOfatom2 = mol.getNodeMap().get("2").getNeighbors();
		
		
		//zda se vztvoril zaznam v listu sousedu pro kazdy atom
		Assert.assertTrue(examp1.equals(ngbOfatom1));
		Assert.assertTrue(examp2.equals(ngbOfatom2));
		
	}
	
	/**
	 *  Test zda metody removeNodes() z tridy Graph vraci spravne vysledky pri pouziti na objekt tridy Molecule 
	 * @throws IOException 
	 * @throws ParseException 
	 */
	@Test
	public void removeNodesTest() {
		Molecule mol = new Molecule();
		mol.addNode("C");
		mol.addNode("C");
		
		mol.connectNodes("1", "2");
		
		mol.removeNode("2");
		
		//test zda byl atom/uzel vymazan z mapy
		Assert.assertFalse(mol.getNodeMap().containsKey("2"));
		
		//test zda byl uzel vymazan s listu sousedu zbyleho atomu
		Assert.assertFalse(mol.getNodeMap().get("1").getNeighbors().contains("2"));
		
	}
	
	/**
	 *  Test zda metody getConnectedComponentNumber z tridy Graph vraci spravne vysledky pri pouziti na objekt tridy Molecule 
	 * @throws IOException 
	 * @throws ParseException 
	 */
	@Test
	public void getConnectedComponentNumberTest() throws ParseException, IOException {
		
		BufferedReader rd;
		rd = new BufferedReader(new FileReader("adenosine.mol"));
		Molecule mol = Molecule.readMolFile(rd);
		int components = mol.getConnectedComponentNumber();
		Assert.assertEquals(1, components);
	}
	
	/**
	 *  Test zda metody getNumberOfCyclesTest() z tridy Graph vraci spravne vysledky pri pouziti na objekt tridy Molecule 
	 * @throws IOException 
	 * @throws ParseException 
	 */
	@Test
	public void getNumberOfCyclesTest() throws ParseException, IOException {
		BufferedReader rd;
		rd = new BufferedReader(new FileReader("adenosine.mol"));
		Molecule mol = Molecule.readMolFile(rd);
		int cycles = mol.getNumberOfCycles();
		Assert.assertEquals(3, cycles);
		
	}
	
	
	/**
	 *  Test zda metody getNodeDistance() z tridy Graph vraci spravne vysledky pri pouziti na objekt tridy Molecule 
	 * @throws IOException 
	 * @throws ParseException 
	 */
	@Test
	public void getNodeDistanceTest() throws ParseException, IOException {
		BufferedReader rd;
		rd = new BufferedReader(new FileReader("aspartic_acid.mol"));
		Molecule mol = Molecule.readMolFile(rd);
		int distance = mol.getNodeDistance("1", "6");
		Assert.assertEquals(4, distance);
		
	}
}
