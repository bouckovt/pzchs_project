package ukol3;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.batik.dom.svg.SVGDOMImplementation;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

/**
 * Trida Molecule rozsiruje tridu Graph o metody pro praci s molekulou, ulozenou jako graf.
 * 
 */
public class Molecule extends Graph{
	private int atomCount = 0;
	
	
	/**
	 * Prekryta metoda addNode, uklada informace o atomu do nodeMap.
	 */
	@Override
	public void addNode(String atomName) {
		atomCount++;
		Atom newAtom;
		
		newAtom = (Atom) nodeMap.get("" + atomCount);
		if (newAtom == null) {
			newAtom = new Atom(atomName);
			newAtom.setAtomIndex(atomCount);
			nodeMap.put("" + atomCount, newAtom);			
		}	
	}
	
	/**
	 * Staticka metoda pro cteni Molfile a vytvoreni instance tridy Molecule z daneho souboru.
	 * 
	 * 
	 * @return molecule
	 * 			Instance tridy Molecule 
	 */
	static Molecule readMolFile(BufferedReader rdr) throws ParseException, IOException {
		Molecule molecule = new Molecule();
		
		
		String sLine;
		ArrayList<ArrayList<String>> rows = new ArrayList<ArrayList<String>>();
		while ((sLine = rdr.readLine()) != null) {
			
			ArrayList<String> row = new ArrayList<String>();
			for (String s : sLine.split(" ")) {
				if (!s.equals("")) {
					row.add(s);
				} 					
			}
			rows.add(row);			  	
		}
		
		String numberOfAtoms = rows.get(3).get(0); //zjisteni poctu atom v molfile
		String numberOfBonds = rows.get(3).get(1); //zjisteni poctu vazeb v molfile
		
		int b = 1;
		
		// nacteni atomu a vazeb
		for (int i=4; i < rows.size(); i++) {
			
			ArrayList<String> row = rows.get(i);
			ArrayList<Float> coordinates3D = new ArrayList<Float>();
			
			if (i < 4 + Integer.parseInt(numberOfAtoms)) {
												
				coordinates3D.add(Float.parseFloat(row.get(0)));
				coordinates3D.add(Float.parseFloat(row.get(1)));
				coordinates3D.add(Float.parseFloat(row.get(2)));
				
				String atomSymbol = row.get(3); // save as atomSymbol
				String atomIndex = String.valueOf(b); // index of atom
				
				
				molecule.addNode(atomSymbol); //vytvoreni node instance
				
				Atom atom = (Atom) molecule.nodeMap.get(atomIndex);
				atom.setCoordinates3D(coordinates3D);
				atom.setAtomSymbol(atomSymbol);
				atom.setAtomIndex(Integer.parseInt(atomIndex));
				
				b++;
				
			}
			else if (i > 3 + Integer.parseInt(numberOfAtoms) & i < 4 + Integer.parseInt(numberOfAtoms) + Integer.parseInt(numberOfBonds)) {
				
				String atomA = row.get(0);
				String atomB = row.get(1);
				
				molecule.connectNodes(atomA, atomB);
						
			}
		}
		
		return molecule;
	}
	
	/**
	 * Staticka metoda pro zapis ve formatu .dot.
	 * 
	 */
	void writeDotty(Writer wrt) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append("graph G {\n");
		for (String edge : edgesList) {
			String indexA = edge.split("-")[0];
			String indexB = edge.split("-")[1];	
			String atomA = ((Atom) nodeMap.get(indexA)).getAtomSymbol();
			String atomB = ((Atom) nodeMap.get(indexB)).getAtomSymbol();
			
			sb.append("\t" + indexA + " -- " + indexB + ";\n");
			sb.append("\t" + indexA + " [label=\"" + atomA + "\"];\n");
			sb.append("\t" + indexB + " [label=\"" + atomB + "\"];\n");
		}
		sb.append("}");
		String sbStr = sb.toString();
		wrt.write(sbStr);
		wrt.flush();
	}
	
	/**
	 * Staticka metoda pro tvorbu jednoducheho svg. Metoda je optimalizovana pro vykreslovani molekuly nactene z Molfile. 
	 * Rozmery svg jsou dane zavisle na souradnicich atomu.
	 * 
	 * @return molecule
	 * 			Instance tridy Molecule 
	 */
	void writeSVG(Writer wrt) throws IOException {
		
		// Create an SVG document.
	    DOMImplementation impl = SVGDOMImplementation.getDOMImplementation();
	    String svgNS = SVGDOMImplementation.SVG_NAMESPACE_URI;
	    SVGDocument doc = (SVGDocument) impl.createDocument(svgNS, "svg", null);

	    // Create a converter for this document.
	    SVGGraphics2D g = new SVGGraphics2D(doc);
	    
	    // nalezeni min souradnic x/y pro pozdejsi posun souradneho systemu
		Float[] xCoordinates = new Float[nodeMap.values().size()]; 
	    Float[] yCoordinates = new Float[nodeMap.values().size()]; 
	    int a = 0;
	    for (Node node : nodeMap.values()) {
	    	Atom atom = (Atom) node;
	    	xCoordinates[a] = atom.getCoordinates3D().get(0);
	    	yCoordinates[a] = atom.getCoordinates3D().get(1);
	    	a++;	
	    }
	    
	    Float xMinBorder = Collections.min(Arrays.asList(xCoordinates));
	    Float yMinBorder = Collections.min(Arrays.asList(yCoordinates));
	    Float xMaxBorder = Collections.max(Arrays.asList(xCoordinates));
	    Float yMaxBorder = Collections.max(Arrays.asList(yCoordinates));
	    
	    
	    for (String edge : edgesList) {
	    	// pro kazdou vazbu v molekule
	    	String indexA = edge.split("-")[0];
			String indexB = edge.split("-")[1];	
			String atomA = ((Atom) nodeMap.get(indexA)).getAtomSymbol();
			String atomB = ((Atom) nodeMap.get(indexB)).getAtomSymbol();
			
			// vykresleni molekuly
			g.setFont(new Font("TimesRoman", Font.BOLD, 25)); 
			Float xCoordinateA = ((Atom) nodeMap.get(indexA)).getCoordinates3D().get(0);
			Float yCoordinateA = ((Atom) nodeMap.get(indexA)).getCoordinates3D().get(1);
			Float xCoordinateB = ((Atom) nodeMap.get(indexB)).getCoordinates3D().get(0);
			Float yCoordinateB = ((Atom) nodeMap.get(indexB)).getCoordinates3D().get(1);
			
			// transformace souradnic do kladneho souradneho systemu
			int xA = Math.round((xCoordinateA+Math.abs(xMinBorder)+1)*75);
			int yA = Math.round((yCoordinateA+Math.abs(yMinBorder)+1)*75);
			int xB = Math.round((xCoordinateB+Math.abs(xMinBorder)+1)*75);
			int yB = Math.round((yCoordinateB+Math.abs(yMinBorder)+1)*75);
			
			//vykresleni car
			g.drawLine(Math.round(xA+10), Math.round(yA-10), 
						Math.round(xB+10), Math.round(yB-10));
			
			//vykresleni bilych ctvercu pro "zakryti" spoju car
			g.setColor(Color.WHITE);
			g.fillRect(xA, yA-18, 20, 20);
			g.fillRect(xB, yB-18, 20, 20);
			
			//vykresleni jmen atomu
			g.setColor(Color.BLACK);
			g.drawString(atomA, xA, yA);
			g.drawString(atomB, xB, yB);
	    	
	    }
	    
	    //velikost svg, hranice optimalizovane pro vykreslovani struktury podle souradnic v Molfile
	    
	    g.setSVGCanvasSize(new Dimension(Math.round((xMaxBorder - xMinBorder + 1)*80), Math.round((yMaxBorder - yMinBorder + 1)*80)));
	    
	    // Populate the document root with the generated SVG content.
	    Element root = doc.getDocumentElement();
	    g.getRoot(root);
	    g.stream(g.getRoot(root), wrt, false, false);
	    
	    wrt.close();
		
	}
}
