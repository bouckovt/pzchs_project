package ukol3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;



public class Main {

	public static void main(String[] args) throws IOException, ParseException {
		System.out.println("Priklady pouziti tridy Molecule");
		System.out.println();
		Molecule mol = new Molecule();
		System.out.println("Pridani atomů");
		
		mol.addNode("C");
		mol.addNode("C");
		mol.addNode("C");
		mol.addNode("C");
		mol.addNode("O");
		
		
		System.out.println("Mapa atomů ve tvaru indexAtomu=Atom[Rad: cislo]: ");
		
		System.out.println(mol.getNodeMap());
		System.out.println();
		System.out.println("Spojeni atomů:");
	
		
		mol.connectNodes("1", "2");
		mol.connectNodes("2", "3");
		mol.connectNodes("3", "4");
		mol.connectNodes("4", "5");
		mol.connectNodes("5", "1");
		
		
		System.out.println(mol.getNodeMap());
		System.out.println();
		System.out.println("Výpis sousedů atomu: ");
		System.out.println();
		for (Node node : mol.getNodeMap().values()) {
			Atom atom = (Atom) node;
			System.out.println("Atom " + atom.getAtomSymbol() + atom.getAtomIndex() + ": " + atom.getNeighbors());
			
		}
		System.out.println();
		System.out.println("Vymazání atomu");
		
		mol.removeNode("8");
		
		
		System.out.println("Výpis sousedů atomu: ");
		System.out.println();
		for (Node node : mol.getNodeMap().values()) {
			Atom atom = (Atom) node;
			System.out.println("Atom " + atom.getAtomSymbol() + atom.getAtomIndex() + ": " + atom.getNeighbors());
			
		}
		System.out.println();
		System.out.println("Vytvoření svg");
		System.out.println();
	
		
		// priklad nacteni 
		System.out.println("Nacteni molekuly z Molfile - adenosine.mol");
		System.out.println();
		
		BufferedReader rdr;
		rdr = new BufferedReader(new FileReader("adenosine.mol"));
		Molecule adenosin = Molecule.readMolFile(rdr);
		System.out.println("Pocet cyklu v molekule: " + adenosin.getNumberOfCycles());
		System.out.println();
		
		System.out.println("Vypis atomu adenosinu a jejich sousedu");
		System.out.println();
		
		for (Node node : adenosin.getNodeMap().values()) {
			Atom atom = (Atom) node;
			System.out.println("Atom " + atom.getAtomSymbol() + atom.getAtomIndex() + ": " + atom.getNeighbors());
			
		}
		
		System.out.println("Zapis molekuly adenosinu do souboru svg- adenosine_svg.svg ...");
		
		// otevreni writeru
		BufferedWriter br2;
		br2 = new BufferedWriter(new FileWriter("adenosineg.svg"));
		adenosin.writeSVG(br2);
		System.out.println("Zapis hotov.");
		System.out.println();
		
		System.out.println("Zapis molekuly adenosinu do souboru .dot- adenosine_dotty.dot ...");
		
		// otevreni writeru
		BufferedWriter br3;
		br3 = new BufferedWriter(new FileWriter("adenosine_dotty.dot"));
		adenosin.writeDotty(br3);;
		System.out.println("Zapis hotov.");
		System.out.println();
		
	}

}
