package ukol3;


import java.util.ArrayList;

/**
 * Trida uchovavajici informace o vrcholu
 *
 */
public class Node {
	private String name;
	private ArrayList<String> neighbors;
	private String visited;
	private String parent;
	private Integer distance;
	
	/**
	 * Inicializace promnennych pro danou instanci tridy Node
	 *
	 */
	public Node(String nodeName) {
		name = nodeName;
		neighbors = new ArrayList<String>();
		visited = "white";
		parent = null;
		distance = -1;
	}
	
	/**
	 * Prida vrchol do seznamu sousedu.
	 *
	 */
	public void addNeighbor(String neighborName) {		
		if (!neighbors.contains(neighborName)) {			
			neighbors.add(neighborName);
		}		
	}
	
	/**
	 * Odstrani vrchol ze seznamu sousedu.
	 *
	 */
	public void removeNeighbor(String neighborName) {
		if (neighbors.contains(neighborName)) {			
			int indexOfNeighbor = this.neighbors.indexOf(neighborName);
			neighbors.remove(indexOfNeighbor);	
		}
		else {
			System.out.println("Vrchol nenalezen");
		}
	}
	
	/**
	 * Vraci informaci o vrcholu
	 * 
	 * @return output
	 * 			retezec ve formatu Node[order:stupen_vrcholu]
	 */
	public String toString() {
		String output = null;
		output = name + "[Order: " + neighbors.size() + "]";
		return output;
		
	}
	
	/**
	 * Vraci seznam sousedu.
	 */
	public ArrayList<String> getNeighbors() {
		return neighbors;
	}
	
	/**
	 * Vraci jmeno vrcholu.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Vraci jmeno rodice vrcholu.
	 *
	 */
	public String getParent() {
		return parent;
	}
	
	/**
	 * Nastavuje hodnotu rodice.
	 *
	 */
	public void setParent(String parentName) {
		parent = parentName;
	}
	
	/**
	 * Vraci hodnotu vzdalenosti.
	 */
	public int getDistance() {
		return distance;
	}
	
	/**
	 * Nastavuje hodnotu vzdalenosti.
	 *
	 */
	public void setDistance(int distance) {
		this.distance = distance;
	}
	
	/**
	 * Vraci informaci zda byl vrchol navstiven nebo uzavren.
	 *
	 */
	public String getVisited() {
		return visited;
	}
	
	/**
	 * Nastavuje zda byl vrchol navstiven.
	 *
	 */
	public void setVisited(String visited) {
		this.visited = visited;
	}
	

}
