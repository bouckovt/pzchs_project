package ukol3;


import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Trida Graph poskytuje metody pro jednoduchy popis neorientovaneho grafu.
 * @author Tereza Boučková
 *
 */
public class Graph {
	protected Map<String, Node> nodeMap;
	protected List<String> edgesList;
	private Integer numberOfCycles;
	private Integer numberOfConnectedComponents;
	
	
	
	/**
	 * Inicializace promnennych pro danou instanci tridy Graph
	 *
	 */
	public Graph() {
		nodeMap = new HashMap<String, Node>();	
		numberOfConnectedComponents = 0;
		numberOfCycles = 0;
		edgesList = new ArrayList<String>();
	}
	
	public void getAll() {
		for (Node node : nodeMap.values()) {
			System.out.println(node.getName()+": " +node.getNeighbors());
		}
		System.out.println(edgesList);
		
	}
	
	public Map<String, Node> getNodeMap() {
		return nodeMap;
	}
	
	/**
	 * Prida vrchol do slovniku, kde klic je nazev vrcholu a hodnota je instance tridy Node.
	 * 
	 * @param nodeName
	 * 				jmeno pridavaneho vrcholu
	 */
	public void addNode(String nodeName) {
		Node newNode;
		newNode = nodeMap.get(nodeName);
		if (newNode == null) {
			newNode = new Node(nodeName);
			nodeMap.put(nodeName, newNode);			
		}		
	}
	
	/**
	 * Odstrani zadany vrchol a jeho hrany
	 * 
	 * @param nodeName
	 * 				jmeno odebiraneho vrcholu		
	 */
	public void removeNode(String nodeName) {
		if (nodeMap.containsKey(nodeName)) {
			
			List<String> ngbList = nodeMap.get(nodeName).getNeighbors();
			if (ngbList.size() > 0) {
				
				for (int i = 0; i  < ngbList.size(); i++) {
					//odstraneni z listu sousedu
					String ngbNode = ngbList.get(i);
					Node node = nodeMap.get(ngbNode);
					node.removeNeighbor(nodeName);
				}
				
			}
			for (int a = 0; a  < edgesList.size(); a++) {
				//odstraneny z listu vazeb
				String edge = edgesList.get(a);
				if (nodeName.equals(edge.split("-")[0])) {
					edgesList.remove(edge);
					a--;
				}
				else if (nodeName.equals(edge.split("-")[1])) {
					edgesList.remove(edge);
					a--;
				}
			}
			nodeMap.remove(nodeName);
		}
		else {
			System.out.println("Vrchol nenalezen");
		}		
	}
	
	/**
	 * Spoji zadane vrcholy, pokud se vrcholy nevyskytuji v seznamu vrcholu grafu, jsou automaticky pridany.
	 * 
	 * @param nodeName1, nodeName2
	 * 				jmena spojovanych vrcholu
	 */
	public void connectNodes(String nodeName1, String nodeName2) {
		
		if (!nodeMap.containsKey(nodeName1)) {
			this.addNode(nodeName1);
		}
		if (!nodeMap.containsKey(nodeName2)) {
			this.addNode(nodeName2);
		}		
		String edge = nodeName1 + "-" + nodeName2;
		String edgeReversed = nodeName2 + "-" + nodeName1;
	
		if (!edgesList.contains(edge) & !edgesList.contains(edgeReversed)) {
			edgesList.add(edge);
		}			
		nodeMap.get(nodeName1).addNeighbor(nodeName2);
		nodeMap.get(nodeName2).addNeighbor(nodeName1);
		
		}
	
	/**
	 * Metoda znackujici vrcholy a pocitajici pocet cyklu v grafu. Implementace depth-first search (DFS) metody.
	 * 
	 * @paran startNode
	 *				instance tridy Node nahodne vybraneho startovniho vrcholu
	 */
	private void markNodes(Node startNode) {
		
		startNode.setVisited("grey");
		List<String> ngbList = startNode.getNeighbors();
		
		for (String ngbName : ngbList) {
			if (nodeMap.get(ngbName).getVisited() == "white") {								
				nodeMap.get(ngbName).setParent(startNode.getName());
				this.markNodes(nodeMap.get(ngbName));	
			}
			else if (nodeMap.get(ngbName).getVisited() == "grey" & nodeMap.get(ngbName).getName() != startNode.getParent()) {
				numberOfCycles++;
			}
		}
		startNode.setVisited("black");
	}
	
	/**
	 * Vraci pocet spojenych komponent v grafu.
	 *
	 */
	public int getConnectedComponentNumber() {
		HashMap<String, Node> nonMarkedNodes = new HashMap<String, Node>();
		for (String nodeName : nodeMap.keySet()) {			
			if (nodeMap.get(nodeName).getVisited() != "grey" & nodeMap.get(nodeName).getVisited() != "black") {				
				nonMarkedNodes.put(nodeName, nodeMap.get(nodeName));
			}
		}		
		if (nonMarkedNodes.size() > 0) {
			List<Node> nodeInstList = new ArrayList<Node>(nonMarkedNodes.values());
			int randomNodeIndex = new Random().nextInt(nodeInstList.size());
			Node startNode = nodeInstList.get(randomNodeIndex);
			numberOfConnectedComponents++;
			this.markNodes(startNode);
			return getConnectedComponentNumber();
		}
		else {
			
			return numberOfConnectedComponents;
		}		
	}
	
	/**
	 * Vraci pocet cyklu nalezenych v grafu.
	 *
	 */
	public int getNumberOfCycles() {
		getConnectedComponentNumber();
		return numberOfCycles;
	}
	
	/**
	 * Znackuje vrcholy na zaklade jejich nejkratsi vzdalenosti od zadaneho startovniho uzlu.
     * Implementace Dijkstrova algoritmu.
     * 
	 * @param startNode
	 * 				instance tridy Node startovniho vrcholu
	 */
	private void markNodesByDistance(Node startNode) {
		int dist = 0;
		List<Node> forMarking = new ArrayList<Node>(Arrays.asList(startNode));		
		while (forMarking.size() > 0) {
			
			List<Node> ngbList = new ArrayList<Node>();
			for (Node node : forMarking) {
				if (node.getDistance() == -1) {
					node.setDistance(dist);					
				}
				else if (node.getDistance() < dist) {
					continue;
				}				
				for (String ngbName : node.getNeighbors()) {
					if (nodeMap.get(ngbName).getDistance() == -1) {
							ngbList.add(nodeMap.get(ngbName));
					}
				}
			}			
			forMarking = ngbList;
			dist++;
		}	
	}
	
	/**
	 * Vraci nejkratsi vzdalenost node2 od node1.
	 *
	 */
	public int getNodeDistance(String nodeName1, String nodeName2) {
		Node startNode = nodeMap.get(nodeName1);
		markNodesByDistance(startNode);
		int distance = nodeMap.get(nodeName2).getDistance();
		
		return distance;
				
	}
	
	/**
	 * Vraci informaci o grafu.
	 *
	 * @return str
	 * 			retezec ve tvaru Graph[nodes:pocet_vrcholu,edges:pocet_hran]
	 */
	public String toString() {
		String str = "Graph[nodes:" + nodeMap.size() + ",edges:" + edgesList.size() + "]";
		return str;
		
	}
	
	/**
	 * Sparsuje textovou reprezentaci grafu ze vstupnihu streamu ve formatu: pocet_vrcholu;cislo_vrcholu-cislo_vrcholu,....
	 *	
	 * @return graph
	 * 			instnace tridy Graph pro vstupni graf
	 */
	static Graph readGraph(Reader rdr) throws java.io.IOException, java.text.ParseException {
		StringBuilder builder = new StringBuilder();
		int charsRead = -1;
		char[] chars = new char[100];
		do{
		    charsRead = rdr.read(chars,0,chars.length);
		    if(charsRead>0)
		        builder.append(chars,0,charsRead);
		}while(charsRead>0);
		
		String input = builder.toString();
		Graph graph = new Graph();
		String[] str = input.split(";");
		String[] edges = str[1].split(",");
		for (String edge : edges) {
			String[] nodes = edge.split("-");
			graph.addNode(nodes[0]);
			graph.addNode(nodes[1]);
			graph.connectNodes(nodes[0], nodes[1]);
			
		}
		
		return graph;
		
	}
	
	/**
	 * Zapise textovou reprezentaci grafu do writeru ve formatu: pocet_vrcholu;cislo_vrcholu-cislo_vrcholu,...
	 *
	 */
	public void writeGraph(Writer wrt) throws java.io.IOException {
		int numberOfNodes = nodeMap.size();
		StringBuilder sb = new StringBuilder();
		sb.append(numberOfNodes).append(";");
		for (String edge : edgesList) {
			sb.append(edge);
			sb.append(",");
		}
		String sbStr = sb.toString();
		String strOut = sbStr.substring(0, sbStr.length()-1);
		
		wrt.write(strOut);
		wrt.flush();
	}
	
	
	
	
	
	
}
 	
	


