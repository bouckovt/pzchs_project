package ukol3;

import java.util.ArrayList;
import java.util.List;

/**
 * Trida Atom je potomkem tridy Node. Je rozsirena o parametry coordinates, atomSymbol a atomIndex.
 */
public class Atom extends Node{
	private List<Float> coordinates3D;
	private String atomSymbol;
	private Integer atomIndex;

	public Atom(String atomName) {
		super(atomName);
		this.coordinates3D = new ArrayList<Float>();
		this.atomSymbol = atomName;
		
		
	}
	
	/**
	 * Metoda vraci hodnotu promenne coordinates3D.
	 * 
	 * @return coordinates3D
	 * 			hodnota coordinates3D
	 */
	public List<Float> getCoordinates3D() {
		return coordinates3D;
	}
	
	/**
	 * Metoda nastavuje 3D souradnice. Jako parametr prijima souradnice ulozene v Listu float cisel.
	 * 
	 * @param coordinates3D
	 * 			List souradnic x,y,z
	 */
	public void setCoordinates3D(List<Float> coordinates3D) {
		this.coordinates3D = coordinates3D;
	}
	
	/**
	 * Metoda vraci hodnotu atributu atomSymbol.
	 * 
	 * @return atomSymbol
	 * 			hodnota atributu atomSymbol
	 */
	public String getAtomSymbol() {
		return atomSymbol;
	}
	
	/**
	 * Metoda nastavuje hodnotu atributu atomSymbol.
	 * 
	 * @param atomSymbol
	 * 			retezec symbolu atomu
	 */
	public void setAtomSymbol(String atomSymbol) {
		this.atomSymbol = atomSymbol;
	}
	
	/**
	 * Metoda vraci hodnotu atributu atomIndex.
	 * 
	 * @return atomIndex
	 * 			hodnota atributu atomIndex
	 */
	public Integer getAtomIndex() {
		return atomIndex;
	}
	
	/**
	 * Metoda nastavuje hodnotu atributu atomIndex
	 * 
	 * @param atomIndex
	 * 			List souradnic x,y,z
	 */
	public void setAtomIndex(Integer atomIndex) {
		this.atomIndex = atomIndex;
	}
	
	
}
