Ve slozce jsou prilozene potrebne knihovny pro spusteni trid. Jsou to knihovny 
tykajici se tvorby SVG.

V metode pro vykreslovani do SVG souboru readMolFile, jsou souradnice optimalizovany 
predvsim pro souradnice ziskane z Molfile, kde se muzou vyskytovat i zaporne hodnoty, 
(bylo nutne posunout souradny system do kladnych hodnot).

Pro vlastni "vymyslene" souradnice vlastnich atomu metoda readMolFile prevazne nezvlada
vytvorit spravne rozmery svg a tim padem se obrazek molekuly posouva mimo ramecek.