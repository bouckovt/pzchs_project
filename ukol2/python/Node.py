
class Node:
    """Trida uchovavajici informace o vrcholu"""

    def __init__(self, name):
        self.name = name
        self.neighbors = []
        self.visited = "white"
        self.parent = None
        self.distance = None

    def add_neighbor(self, neighbor):
        """Prida vrchol do seznamu sousedu."""
        if neighbor not in self.neighbors:
            self.neighbors.append(neighbor)
        else:
            print "Vrchol %s uz je v seznamu sousedu vrcholu %s" % (neighbor, self.name)


    def remove_neighbor(self, neighbor):
        """Odstrani vrchol ze seznamu sousedu."""
        if neighbor in self.neighbors:
            self.neighbors.remove(neighbor)
        else:
            print "Sousedni vrchol nenalezen"

    def to_string(self):
        """Vraci informaci o vrcholu ve formatu Node[order:stupen_vrcholu]"""
        return "%s[Order: %s]" %(self.name, len(self.neighbors))

    def get_neighbors(self):
        """Vraci seznam sousedu."""
        return self.neighbors

    def get_name(self):
        """Vraci jmeno vrcholu."""
        return self.name

    def get_parent(self):
        """Vraci jmeno rodice vrcholu."""
        return self.parent

    def set_parent(self, parent):
        """Nastavuje hodnotu rodice."""
        self.parent = parent

    def get_distance(self):
        """Vraci hodnotu vzdalenosti."""
        return self.distance

    def set_distance(self, distance):
        """Nastavuje hodnotu vzdalenosti."""
        self.distance = distance

    def get_visited(self):
        """Vraci informaci zda byl vrchol navstiven nebo uzavren."""
        return self.visited

    def set_visited(self, color):
        """Nastavuje zda byl vrchol navstiven."""
        self.visited = color


