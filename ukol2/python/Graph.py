from Node import Node
import random


class Graph:
    """Trida Graph poskytuje metody pro jednoduchy popis neorientovaneho grafu"""

    def __init__(self):
        """Konstruktor tridy Graph"""
        self.nodes_dict = {}
        self.edges = []
        self.component_number = 0
        self.number_of_cycles = 0


    def add_node(self, node_name):
        """Prida vrchol do slovniku, kde klic je nazev vrcholu a hodnota je instance tridy Node"""
        if node_name not in self.nodes_dict.keys():
            new_node = Node(node_name)
            self.nodes_dict[node_name] = new_node
        else:
            print "Zadany vrchol je uz v seznamu."

    def remove_node(self, node_name):
        """Odstrani zadany vrchol a jeho hrany"""
        if node_name in self.nodes_dict.keys():
            neighbors = self.nodes_dict[node_name].get_neighbors()
            if len(neighbors) > 0:
                for neighbor in neighbors:
                    for edge in self.edges:
                        if node_name == edge[0]:
                            self.edges.remove(edge)
                        elif node_name == edge[1]:
                            self.edges.remove(edge)
                    self.nodes_dict[neighbor].remove_neighbor(node_name)
            del self.nodes_dict[node_name]
        else:
            print "Vrchol nenalezen"

    def connect_nodes(self, node_name1, node_name2):
        """Spoji zadane vrcholy, pokud se vrcholy nevyskytuji v seznamu vrcholu grafu, jsou automaticky pridany."""
        if node_name1 not in self.nodes_dict.keys():
            self.add_node(node_name1)
        if node_name2 not in self.nodes_dict.keys():
            self.add_node(node_name2)

        edge = node_name1 + node_name2
        if edge not in self.edges and edge[::-1] not in self.edges:
            self.edges.append(edge)

        self.nodes_dict[node_name1].add_neighbor(node_name2)
        self.nodes_dict[node_name2].add_neighbor(node_name1)

    def to_string(self):
        """Vraci informaci o grafu ve tvaru Graph[nodes:pocet_vrcholu,edges:pocet_hran]"""
        return "Graph[nodes: %s, edges: %s]" %(len(self.nodes_dict), len(self.edges))

    def mark_nodes(self, start_node):
        """Metoda znackujici vrcholy a pocitajici pocet cyklu v grafu. Implementace depth-first search (DFS) metody"""
        start_node.set_visited("grey")
        nb = start_node.get_neighbors()
        for node in nb:
            if self.nodes_dict[node].get_visited() == "white":
                self.nodes_dict[node].set_parent(start_node.get_name())
                self.mark_nodes(self.nodes_dict[node])
            elif self.nodes_dict[node].get_visited() == "grey" and self.nodes_dict[node].get_name() != start_node.get_parent():
                self.number_of_cycles += 1
        start_node.set_visited("black")

    def get_connected_components_number(self):
        """Vraci pocet spojenych komponent v grafu."""
        non_marked_nodes = {}
        for node in self.nodes_dict.items():
            if node[1].get_visited() not in ("grey", "black"):
                non_marked_nodes.update({node[0]: node[1]})
        if len(non_marked_nodes) > 0:
            start_node = random.choice(non_marked_nodes.values())
            self.component_number += 1
            self.mark_nodes(start_node)

            return self.get_connected_components_number()
        else:

            return self.component_number

    def get_number_of_cycles(self):
        """Vraci pocet cyklu nalezenych v grafu."""
        self.get_connected_components_number()

        return self.number_of_cycles

    def mark_nodes_by_dist(self, start_node):
        """
        Znackuje vrcholy na zaklade jejich nejkratsi vzdalenosti od zadaneho startovniho uzlu.
        Implementace Dijkstrova algoritmu.
        """
        dist = 0
        for_marking = [start_node]
        while for_marking:
            nb = []
            for node in for_marking:
                if node.get_distance() is None:
                    node.set_distance(dist)
                elif node.get_distance() < dist:
                    continue
                for ngb in node.neighbors:
                    if self.nodes_dict[ngb].get_distance() is None:
                        nb.append(self.nodes_dict[ngb])
            for_marking = nb
            dist += 1

    def get_node_distance(self, node1, node2):
        """Vraci nejkratsi vzdalenost node2 od node1"""
        start_node = self.nodes_dict[node1]
        self.mark_nodes_by_dist(start_node)
        distance = self.nodes_dict[node2].get_distance()

        return distance

    def read_graph(self, graph):
        """Sparsuje textovou reprezentaci grafu ve formatu: pocet_vrcholu;cislo_vrcholu-cislo_vrcholu,...."""
        graph = graph.split(";")
        edges = graph[1].split(",")
        for edge in edges:
            nodes = edge.split("-")
            self.add_node(nodes[0])
            self.add_node(nodes[1])
            self.connect_nodes(nodes[0], nodes[1])

    def write_graph(self, file_name):
        """
        Zapise textovou reprezentaci grafu do zadaneho souboru
        ve formatu: pocet_vrcholu;cislo_vrcholu-cislo_vrcholu,...
        """
        try:
            output_file = open(file_name, "w")
            number_of_nodes = len(self.nodes_dict)
            connection = []
            for edge in self.edges:
                connection.append("-".join([edge[0], edge[1]]))
            output_text = str(number_of_nodes) + ";" + ",".join(connection)
            output_file.write(output_text)
            output_file.close()
        except IOError:
            print "Spatny soubor"





