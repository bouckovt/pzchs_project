from rdkit import Chem
import glob
from rdkit.Chem import Mol
import fces

# nacteni jmen .SMI souboru ze slozky data
list_of_smiles_files = glob.glob("data/*.smi")

# zakladni zpracovani sloucenin, prevedeni do inchi, ukol 4.2 a priprava dat pro 4.4
fces.processing(list_of_smiles_files)

# ziskani vyfiltrovanych sloucenin pro 4.4
filtered_list = fces.filter_substructure(list_of_smiles_files)

# serazeni sloucenin podle jejihc molarni hmotnosti
sorted_list = fces.sort_mols(filtered_list)

ukol4_4 = open("results4_4.smi", "w")
# zapis do souboru
for struct in sorted_list:
    wrt = struct + " " + str(fces.get_mol_wt(struct)) + "\n"
    ukol4_4.write(wrt)

