from rdkit import Chem
from rdkit.Chem import Descriptors
from rdkit.Chem import Mol
import glob

def processing(list_of_smiles_files):
    """
    Zakladni pruchod daty
    -funkce vezme seznam .smi souboru, prevede smiles na inchi,
    -ulozi do souboru results4_2.smi, molekuly s nitrilovou skupinou a molWT mensi nez 150 g/mol
    -ulozi do souboru data_for_4_4.smi, molekuly s 4 a m0n2 atomy, urcene pro dalsi zpracovani
    """
    ukol4_2 = open("results4_2.smi", "w")
    data_for_4_4 = open("data_for_4_4.smi", "w")

    for file_path in list_of_smiles_files:

        smi_file = open(file_path, "r")
        file_name = file_path.split("\\")[1]

        inchi_file_path = "data/" + file_name.replace(".smi", ".inchi")
        inchi_file = open(inchi_file_path, "w")

        for line in smi_file:

            smiles = line.split()[0]
            index = line.split()[1]
            molecule = Chem.MolFromSmiles(smiles)

            if molecule is None:
                continue

            # smiles to inchi
            inchi = Chem.MolToInchi(molecule) + " " + index + "\n"
            inchi_file.write(inchi)

            # molekuly obsahujici -CN skupinu a s molWT mensi nez 150 g/mol

            mol_wt = Descriptors.MolWt(molecule)
            smarts_CN = Chem.MolFromSmarts("[NX1]#[CX2]")

            if molecule.HasSubstructMatch(smarts_CN) and mol_wt <= 150:
                ukol4_2.write(line)

            #molekuly do 4 atomu
            num_atoms = Mol.GetNumAtoms(molecule)
            if num_atoms <= 4:
                data_for_4_4.write(line)

        inchi_file.close()

    ukol4_2.close()
    data_for_4_4.close()

def get_mols_molwt_less_than(molwt):
    """Najde vsechny molekuly s nizsi molarni hmotnosti nez zadana hmotnost"""
    list_of_smiles_files = glob.glob("data/*.smi")
    list_of_filtered_mols = []

    for file_path in list_of_smiles_files:

        smi_file = open(file_path, "r")
        for line in smi_file:

            smiles = line.split()[0]
            molecule = Chem.MolFromSmiles(smiles)

            if molecule is None:
                continue

            mol_wt = Descriptors.MolWt(molecule)
            if mol_wt <= molwt:
                list_of_filtered_mols.append(smiles)
                print smiles, mol_wt

    return list_of_filtered_mols


def filter_substructure(list_of_smiles_files):
    """Vyfiltruje molekuly ze seznamu molekul s mene nez 4 atomy, ktere nejsou substrukturou jine slouceniny"""
    structures = open("data_for_4_4.smi", "r")
    structure_list = []
    for structure in structures:
        struct_smiles = structure.split()[0]
        structure_list.append(struct_smiles)

    positive_hits = []
    for file_path in list_of_smiles_files:
        smi_file = open(file_path, "r")

        for line in smi_file:
            other_smiles = line.split()[0]
            other_mol = Chem.MolFromSmiles(other_smiles)
            if other_mol is None:
                continue
            #print line.split()[1]
            for structure in structure_list:
                struct_mol = Chem.MolFromSmiles(structure) # molekula predfiltrovanych sloucenin atom <=4
                if structure != other_smiles and other_mol.HasSubstructMatch(struct_mol):
                    if structure not in positive_hits:
                        structure_list.remove(structure)
                        positive_hits.append(structure)  # prida do seznamu molekulu pokud je substrukturou

        print "pocet struktur ", len(structure_list)
        print "END OF FILE", file_path
        smi_file.close()

    structures.close()
    return structure_list


def get_mol_wt(smiles):
    """"Pomocna funkce pro serazeni vyslednych molekul"""
    mol = Chem.MolFromSmiles(smiles)
    mol_wt = Descriptors.MolWt(mol)
    return mol_wt


def sort_mols(structure_list):
    """Vrati seznam serazenych molekul podle jejich molarni hmotnosti"""
    return sorted(structure_list, key=get_mol_wt)