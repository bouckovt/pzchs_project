from SumarniVzorec import SumarniVzorec
import argparse


parser = argparse.ArgumentParser(description="""
Skript pro vyzkouseni metod tridy SumarniVzorec""")


parser.add_argument("-va", action='store_true', help="zobrazi vsechny atomy")
parser.add_argument("-p", nargs=1, help="zobrazi pocet zadaneho atomu v dane molekule", metavar="atom")
parser.add_argument("-hf", action='store_true', help="zobrazi retezec molekuly v Hillove zapisu")
parser.add_argument("-ext", nargs=1, help="prida ke vzorci A atomy vzorce B a zobrazi vysledny vzorec v Hillove zapisu", metavar="vzorecB")
parser.add_argument("vzorec", help="vzorec molekuly")

arg = parser.parse_args()
vzorec = parser.parse_args().vzorec
molekula1 = SumarniVzorec(vzorec)

if arg.va:
    print "Seznam vsech atomu: " + str(molekula1.vsechny_atomy())

if arg.p:
    print "Pocet atomu %s je %s." %(arg.p[0], molekula1.pocet(arg.p[0]))

if arg.hf:
    print "Hilluv zapis pro vzorec %s je %s" %(vzorec, molekula1.to_string())

if arg.ext:
    molekula2 = SumarniVzorec(arg.ext[0])
    molekula1.extend(molekula2)
    print "Puvodni molekula %s" %(vzorec)
    print "Vysledny vzorec po pridani vzorce %s je %s" %(arg.ext[0], molekula1.to_string())





