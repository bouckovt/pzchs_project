
class SumarniVzorec:
    """Trida pro ukladani informaci o sumarnim vzorci molekuly."""

    def __init__(self, vzorec):
        self.vzorec = vzorec
        self.atomy = self.mapa_atomu()

    def mapa_atomu(self):
        """Vytvori slovnik z retezce, kde klic je atom a hodnota je pocet atomu daneho atomu"""
        cislo_atomu = ""
        atom = ""
        atomy = {}
        i = 0

        while i < len(self.vzorec):
            if self.vzorec[i].isalpha():
                atom += self.vzorec[i]
                if i != len(self.vzorec) - 1:
                    #pokud je nasledujici znak velke pismeno, nastavi pocet atomu na 1
                    if self.vzorec[i+1].isupper():
                        atomy[atom] = "1"
                        atom = ""
                else:
                    atomy[atom] = "1"
                i += 1

            elif self.vzorec[i].isdigit():
                cislo_atomu += self.vzorec[i]
                if i != len(self.vzorec) - 1:
                    if self.vzorec[i+1].isalpha():
                        atomy[atom] = cislo_atomu
                        cislo_atomu = ""
                        atom = ""
                else:
                    atomy[atom] = cislo_atomu
                    cislo_atomu = ""
                    atom = ""
                i += 1
        return atomy

    def vsechny_atomy(self):
        """Vraci seznam atomu v molekule."""
        atomy = self.atomy.keys()
        return atomy

    def pocet(self, jmeno_atomu):
        """Vraci pocet zadaneho atomu"""
        pocet_atomu = self.atomy.get(jmeno_atomu, "Atom nenalezen")
        return pocet_atomu

    def to_string(self):
        """Vraci retezec sumarniho vzorce v Hillove zapisu."""
        atomy = self.atomy
        retezec = ""
        uhlik = ""
        vodik = ""
        for atom in sorted(atomy):
            if atomy[atom] == "1":
                atomy[atom] = ""

            if atom == "C":
                uhlik = "C" + atomy[atom]
            elif atom == "H":
                vodik = "H" + atomy[atom]
            else:
                retezec = retezec + atom + atomy[atom]

        return uhlik + vodik + retezec



    def extend(self, SumarniVzorec):
        """"Prida atomy k vlastnimu vzorci z druheho vzorce """
        atomy_molekula_2 = SumarniVzorec.mapa_atomu()

        for atom_mol2 in atomy_molekula_2.keys():

            if atom_mol2 in self.atomy.keys():

                self.atomy[atom_mol2] = str(int(atomy_molekula_2[atom_mol2]) + int(self.atomy[atom_mol2]))

            else:
                self.atomy[atom_mol2] = str(atomy_molekula_2[atom_mol2])


