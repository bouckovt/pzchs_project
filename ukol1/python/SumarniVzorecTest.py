import unittest
from SumarniVzorec import *

class Vystupy(unittest.TestCase):
    """Testovaci trida pro SumarniVzorec.py """

    def setUp(self):
        """Vytvoreni testovacich objektu. """
        self.mol1 = SumarniVzorec("O3H19NC17")
        self.mol2 = SumarniVzorec("H15C10ON")
        self.mol3 = SumarniVzorec("AsO3")

    def test_mapa_atomu(self):
        """Metoda mapa_atomu by mela vracet slovnik kde klic je atom a hodnota pocet tohoto atomu."""
        slovnik = self.mol1.mapa_atomu()
        self.assertEqual(slovnik, {"O": "3", "H": "19", "N": "1", "C": "17"})

    def test_vsechny_atomy(self):
        """Metoda vsechny_atomy by mela vracet seznam vsech atomu v molekule"""
        vystup1 = self.mol1.vsechny_atomy()
        vystup1.sort()
        vystup3 = self.mol3.vsechny_atomy()
        vystup3.sort()
        self.assertEqual(vystup1, ["C", "H", "N", "O"])
        self.assertEqual(vystup3, ["As", "O"])

    def test_pocet(self):
        """Metoda pocet by mela vracet pocet zadaneho atomu v molekule."""
        pocet_atomu = self.mol1.pocet("H")
        self.assertEqual(pocet_atomu, "19")

    def test_pocet2(self):
        """Metoda pocet by mela vratit 'Atom nenelezen' v pripade, ze zadany atom se nenachazi v molekule."""
        nenalezen = self.mol1.pocet("R")
        self.assertEqual(nenalezen, "Atom nenalezen")

    def test_to_string(self):
        """Metoda to_string by mela vratit retezec sumarniho vzorce v Hillove zapisu"""
        hilluv_zapis = self.mol1.to_string()
        self.assertEqual(hilluv_zapis, "C17H19NO3")

    def test_extend(self):
        """Metoda extend by mela pridat atomy molekuly B k molekule A"""
        self.mol1.extend(self.mol2)
        self.assertEqual(self.mol1.atomy, {"H": "34", "C": "27", "O": "4", "N": "2"})



if __name__ == '__main__':
    unittest.main()